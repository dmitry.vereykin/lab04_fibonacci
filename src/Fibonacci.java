/**
 * Created by Dmitry Vereykin on 10/2/2015.
 */

import java.util.*;

public class Fibonacci {

    public static void main(String[] args) {
        int number;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("How many numbers in the sequence? ");
        number = keyboard.nextInt();

        System.out.println("--->");

        for (int i = 1; i <= number; i++)
            System.out.print(fibRec(i) + " ");

        System.out.println("\n--->");
        fibNonRec(number);
    }

    public static long fibRec(int n) {
        if(n == 1 || n == 2)
            return 1;
        else
            return fibRec(n - 2) + fibRec(n - 1);
    }

    public static void fibNonRec(int n) {
        long fib = 1;
        long fibPrev = 1;

        if (n >= 1)
           System.out.print(fib + " ");

        if (n >= 2)
            System.out.print(fib + " ");

        for (int i = 2; i < n; i++) {
            long temp = fib;
            fib = fib + fibPrev;
            System.out.print(fib + " ");
            fibPrev = temp;
        }
    }
}
